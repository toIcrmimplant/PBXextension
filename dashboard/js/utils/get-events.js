import {event} from "./templates.js";
const eventsTable = document.getElementById('events-table')

export function getEvents(obj) {
    const to = obj.call_to.substr(0, obj.call_to.indexOf("@")) || obj.call_to
    const date = obj.created
    const caller = obj.call_from.substr(0, obj.call_from.indexOf("@")) || obj.call_from
    const status = obj.direction
    const link = obj.recording_url

    eventsTable.insertRow(0).innerHTML = event(to, date, caller, status, link)
}
