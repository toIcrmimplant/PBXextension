const event = (to, date, caller, status, link) => `
    <tr>
        <div class="list-group-item mt-2" style="background-color: #f1f3f4">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">${to}</h5>
                <small>${date}</small>
            </div>
            <div class="d-flex w-100 justify-content-between">
            <div>
                <p class="mb-1 cursor-pointer"><div class="btn-group dropend">${caller}</div></p>
                <small class="text-capitalize">${status}</small>
            </div>
            <audio controls>
              <source src="${link}" type="audio/x-wav">
              Your browser does not support the audio tag.
            </audio>
            </div>
        </div>
    </tr>`


const info = (company, balance) => `
    <div>Hello, ${company}!</div>
    <div>Balance: $<span>${balance}</span></div>`


export {event, info}

