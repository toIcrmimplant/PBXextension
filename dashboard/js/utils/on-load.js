import {getEvents} from './get-events.js'

chrome.storage.sync.get(['auth_key'], async data => {
    await axios.post('https://app.pbximplant.com/calls-logs_api/call-stats', {
        auth_key: data.auth_key,
        path: 0
    }).then(res => res.data.reverse())
        .then(events => {
            events.map(obj => {
                getEvents(obj)
            })
        }).catch(e => {
            console.log(e)
        })
})
