import {getEvents} from './get-events.js'

const eventsOuter = document.getElementById('events-outer')
const spinner = document.getElementById('spinner')

let onRequest = false
let pageNumber = 1

eventsOuter.addEventListener('scroll', () => {
    if (pageNumber === 50) return spinner.innerHTML = ""

    if (Math.ceil(eventsOuter.scrollTop) + eventsOuter.clientHeight >= eventsOuter.scrollHeight) {

        if (onRequest) {
            return (async () => await setTimeout(() => onRequest = false, 500))()
        }

        chrome.storage.sync.get(['auth_key'], async data => {
            await axios.post('https://app.pbximplant.com/calls-logs_api/call-stats', {
                auth_key: data.auth_key,
                path: pageNumber
            }).then(res => {
                res.data.reverse().map(obj => {
                    getEvents(obj)
                })
                onRequest = true
                eventsOuter.scroll(0, eventsOuter.scrollTop - 500)
            })
        })
        pageNumber++
        console.log(pageNumber)
    }
})

export const infiniteScroll = () => eventsOuter
