const localAudio = document.getElementById('local-audio')

navigator.mediaDevices.getUserMedia({audio: true})
    .then(stream => localAudio.srcObject = stream)
    .catch(e => console.log(e))

const device = new Twilio.Device

chrome.storage.sync.get(['call_info'], data => {
    device.setup(data.call_info.token, {debug: true})
})

await device.error(() => {
    chrome.storage.sync.get(['auth_key'], async (data) => {
        await axios.post('https://crmpbx.app/users_api/get-twilio-app-token', {
            auth_key: data.auth_key
        }).then(res => chrome.storage.sync.set({call_info: res.data}))
    })
})


export const callTo = number => device.connect({To: number})
export const hangupCall = () => device.disconnectAll();
export const exportDevice = device
