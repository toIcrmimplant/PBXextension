import {getEvents} from "../utils/get-events.js";

const socket = io('ws://localhost:5000/api/events')

socket.on('connect', () => {
    chrome.storage.sync.get(['company_info'], company_info => {
        chrome.storage.sync.get(['user_info'], user_info => {
            socket.emit('extensionCreate', {
                accountSid: company_info.company_info.twilio_sid,
                userSid: user_info.user_info.sid
            })
        })
    })

    socket.on('create', obj => {
        getEvents(obj)
    })
})

export const sockets = () => socket
