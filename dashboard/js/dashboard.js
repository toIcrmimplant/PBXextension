import {sockets} from './pages/call-logs.js'
import {infiniteScroll} from "./utils/infinite-scroll.js"
import {callTo, exportDevice, hangupCall} from "./pages/calls.js"
import {init} from "./pages/settings.js";

const callsAreaBtn = document.getElementById('calls-area')
const callLogsAreaBtn = document.getElementById('call-logs-area')
const settingsAreaBtn = document.getElementById('settings-area')

const callsPage = document.getElementById('calls')
const logsPage = document.getElementById('logs')
const settingsPage = document.getElementById('settings')

const call = document.getElementById('call')
const hangup = document.getElementById('hangup')
const number = document.getElementById('number')

// Handlers
function eventFire(el, e_type) {
    if (el.fireEvent) {
        el.fireEvent('on' + e_type);
    } else {
        const evObj = document.createEvent('Events');
        evObj.initEvent(e_type, true, false);
        el.dispatchEvent(evObj);
    }
}

function getUpWindows() {
    chrome.storage.local.get(['windows'], res => {
        chrome.windows.update(res.windows[0].id, {
            focused: true,
            drawAttention: true
        })
    })
}

function nullify() {
    callsAreaBtn.classList.remove('active')
    callLogsAreaBtn.classList.remove('active')
    settingsAreaBtn.classList.remove('active')

    callsPage.classList.remove('active-tab')
    logsPage.classList.remove('active-tab')
    settingsPage.classList.remove('active-tab')
}

//Calls runtime handlers
await exportDevice.incoming(connection => {
    const callAccept = document.getElementById('accept-call')
    const callReject = document.getElementById('reject-call')
    const Modal = new bootstrap.Modal(document.getElementById('staticBackdrop'), {focus: true})

    getUpWindows()
    Modal.show()

    callAccept.addEventListener('click', () => {
        connection.accept()
    })
    callReject.addEventListener('click', () => {
        connection.ignore()
        Modal.hide()
    })
})
chrome.runtime.onMessage.addListener(mes => {
    if (mes.type === 'call') {
        getUpWindows()
        eventFire(callsAreaBtn, 'click')

        number.value = mes.number

        console.log(mes.number)
        // callTo(mes.number)
    }
})

//SPA
callsAreaBtn.addEventListener('click', e => {
    e.preventDefault()
    nullify()
    callsAreaBtn.classList.add('active')
    callsPage.classList.add('active-tab')

    call.addEventListener('click', () => {
        console.log(number.value)
        callTo(number.value)
    })

    hangup.addEventListener('click', () => {
        number.value = ''
        hangupCall()
    })
})
callLogsAreaBtn.addEventListener('click', e => {
    e.preventDefault()
    nullify()
    callLogsAreaBtn.classList.add('active')
    logsPage.classList.add('active-tab')
    sockets()
    infiniteScroll()
})
settingsAreaBtn.addEventListener('click', e => {
    e.preventDefault()
    nullify()
    settingsAreaBtn.classList.add('active')
    settingsPage.classList.add('active-tab')
    init()
})

//Ready SPA
window.onload = () => {
    eventFire(settingsAreaBtn, 'click')
}
