﻿const submit = document.getElementById('submit')
const apiKeyInput = document.getElementById('api-key')
const name = document.getElementById('name')
const sip = document.getElementById('sip')
const problemsWithKey = document.getElementById('problems-with-key')
const formContainer = document.getElementById('form-container')
const loggedModal = document.getElementById('logged-modal')
const newTab = document.getElementById('new-tab')
const logoutBtn = document.getElementById('logout')

function login() {
    formContainer.classList.add('d-none')
    loggedModal.classList.remove('d-none')
    chrome.action.setIcon({path: '/images/logo38.png'})
    chrome.storage.sync.get(['user_info'], data => {
        console.log(data.user_info)
        name.innerText = data.user_info.name
        sip.innerText = data.user_info.sip
    })
}

function createTab() {
    chrome.tabs.create({
        url: 'dashboard/dashboard.html',
        "active": false
    }, tab => {
        chrome.windows.create({
            tabId: tab.id,
            type: 'popup',
            focused: true,
            state: 'normal',
            height: 720,
            width: 1080
        }, window => {
            console.log(window)
            chrome.storage.local.get(['windows'], res => {
                if (!res.windows)
                    return chrome.storage.local.set({windows: [window]})
                chrome.storage.local.set({windows: [window, ...res.windows]})
            })
        })
    })
}

chrome.storage.sync.get(['auth_key'], data => {
    if (data.auth_key) return login()

    submit.addEventListener('click', async event => {
        event.preventDefault()
        const key = apiKeyInput.value

        await axios.post('https://app.pbximplant.com/users_api/get-user-pbx', {
            auth_key: key
        }).then(async res => {
            chrome.storage.sync.set({auth_key: key, user_info: res.data})
            await axios.post('https://crmpbx.app/users_api/get-twilio-app-token', {
                auth_key: key
            }).then(res => chrome.storage.sync.set({call_info: res.data}))
            await axios.post('https://app.pbximplant.com/pay/get-company-data', {
                auth_key: key
            }).then(res => chrome.storage.sync.set({company_info: res.data}))
            createTab()
            login()
        }).catch(() => {
            apiKeyInput.classList.add('is-invalid')
            problemsWithKey.innerText = 'Invalid API key'
            return apiKeyInput.value = ''
        })
    })
})

newTab.addEventListener('click', () => {
    createTab()
})

logoutBtn.addEventListener('click', e => {
    e.preventDefault()

    chrome.storage.local.get(['windows'], res => {
        res.windows.map(window => chrome.windows.remove(window.id))
        chrome.storage.local.set({windows: []})
    })

    setTimeout(() => {
        chrome.storage.sync.clear()
        chrome.storage.local.clear()
        window.close()
    }, 1000)
})
