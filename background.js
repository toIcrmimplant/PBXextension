chrome.storage.local.set({windows: []})
chrome.storage.sync.get(['auth_key'], data => {
    if (data.auth_key) {
        chrome.action.setIcon({path: '/images/logo38.png'})
    }
})

chrome.tabs.onUpdated.addListener(() => {
    chrome.tabs.query({
        active: true,
        currentWindow: true,
        url: "https://*/leads/detail*"
    }, tabs => {
        tabs.map(tab => {
            if (tab.status === 'complete') {
                chrome.scripting.executeScript({
                    files: ["./click-to-call.js"],
                    target: {
                        tabId: tab.id
                    }
                })
            }
        })
    })
})
